/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : hd

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-12-21 17:09:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for blogs
-- ----------------------------
DROP TABLE IF EXISTS `blogs`;
CREATE TABLE `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `blogs_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of blogs
-- ----------------------------
INSERT INTO `blogs` VALUES ('1', 'Dolores et id suscipit ducimus. Et animi consectetur quia culpa magnam qui dolorem.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('2', 'Ratione quod nesciunt et sed enim. Voluptatem et commodi vero veritatis non qui.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('3', 'Voluptas qui qui dolore quaerat vel nihil. Id inventore eum aut modi nostrum quia magnam.', '1', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('4', 'Tempora dolorem porro quis facere earum. Sed fugiat voluptas fuga veritatis quasi.', '3', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('5', 'Eveniet vero alias excepturi velit et sunt aut doloribus. Ut modi eos vel et qui in.', '1', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('6', 'Nobis quo adipisci doloremque architecto. Quo delectus beatae aut. Illo quibusdam harum dicta quia.', '1', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('7', 'Quae dolores et debitis dicta omnis ut. Maxime sunt repellat qui veniam.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('8', 'Et sint qui possimus animi consequatur. Quos quam magnam neque explicabo necessitatibus.', '3', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('9', 'Cumque aut nihil dolor autem. Nisi ea voluptate sed voluptas corrupti eaque laboriosam repellat.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('10', 'Ut in ratione sed et. Qui voluptatem maiores dolorum eligendi. Et alias voluptatem commodi ex.', '1', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('11', 'Unde in voluptates a velit quia non dolores. Ducimus eum ex magni.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('12', 'Deserunt omnis illo explicabo sed. Sit asperiores natus voluptate minima molestiae nesciunt.', '1', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('13', 'Sequi aut excepturi est. Ut atque amet ut molestiae. Possimus velit velit asperiores.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('14', 'Ullam ut ea deleniti quia. Vitae eos natus et. Omnis ducimus dolores et reiciendis aut.', '1', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('15', 'Accusantium sunt eius sint tempore sed sed vel. Dolor modi in aliquid excepturi.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('16', 'Nesciunt veniam quaerat amet tempora veritatis ut reiciendis. Aut natus enim sunt nihil est.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('17', 'Expedita omnis ut assumenda vel. Laborum et error consequuntur quia. Aut quis earum quisquam et.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('18', 'Aut aut deleniti ea quo. Quam natus est eius quidem incidunt. Veniam voluptatem eveniet pariatur.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('19', 'Culpa quia ut et nisi. Quidem possimus a assumenda. Et rerum eligendi nobis facilis.', '3', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('20', 'Dignissimos dolores autem ut quo. Tempora quis qui nulla ea at.', '1', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('21', 'Vel et porro qui a quia voluptas ipsum dolore. Neque voluptatem aut eaque. Magnam vero sunt et.', '1', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('22', 'Sunt nulla unde aliquid maiores rerum. Et rerum ut sequi.', '1', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('23', 'Officiis quisquam soluta nostrum aspernatur in. Nesciunt assumenda eum laborum porro eaque aut et.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('24', 'Quo nobis veniam nihil at. Exercitationem magni harum soluta ut sunt. Quis adipisci deleniti eaque.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('25', 'Expedita architecto aliquid illo. Et tempora repellat dignissimos.', '3', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('26', 'Voluptatem quasi unde animi soluta nostrum provident voluptas sed. Qui praesentium et facere.', '1', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('27', 'Molestiae nesciunt blanditiis commodi enim. Qui perferendis sint eius id ipsa corrupti officiis.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('28', 'Aut ipsum consequatur totam. Dolores vel qui aspernatur. Est sit eum asperiores nesciunt.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('29', 'Minus ullam officiis rerum eos modi et corrupti. Eius sed quisquam facere aperiam architecto.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `blogs` VALUES ('30', 'Provident et delectus non. Fuga qui provident et architecto qui qui. Velit sed cumque ut.', '2', '2019-12-21 16:05:30', '2019-12-21 16:05:30');

-- ----------------------------
-- Table structure for follows
-- ----------------------------
DROP TABLE IF EXISTS `follows`;
CREATE TABLE `follows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `follower` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of follows
-- ----------------------------
INSERT INTO `follows` VALUES ('2', '6', '1', '2019-12-21 16:36:40', '2019-12-21 16:36:40');
INSERT INTO `follows` VALUES ('3', '9', '1', '2019-12-21 16:36:50', '2019-12-21 16:36:50');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('18', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('19', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('20', '2019_12_21_145700_create_blogs_table', '1');
INSERT INTO `migrations` VALUES ('21', '2019_12_21_160317_create_table_follows', '1');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_active` tinyint(4) NOT NULL DEFAULT '0',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', '1073298557@qq.com', 'a5rnO2Rcx1', '0', '$2y$10$u2YT.CCBQ1XIZIWuY9UOB.TToJLy/W3rxnfOIJfiu6/lthOVQZmoe', '1', '2S3lxTWwoq', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('2', 'Presley Greenholt', 'harris.vivianne@example.com', 'pEcmiMwxJv', '0', '$2y$10$jNb15w3jJBy8ZGAIJMANPeSMcjTy2hVwhz4h4HXQI7xffb9NYD2x2', '0', 'G2boGbf3VP', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('3', 'Dominic Hartmann', 'ozulauf@example.com', '4AJzgeYfZm', '0', '$2y$10$N0bScdQFMXxSVzKlV/aJTuf45Xf/TrZP9cCd0m3ejcYYDxW8fFV5W', '0', 'Vq3tdXFw7v', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('4', 'Summer Wiza', 'egoldner@example.org', 'KoiIthPDvu', '0', '$2y$10$5dqpiBzTmGRq9lNkTN6gh..n9y5a6zv0SPoQbTfx5dxX7wPHULXni', '0', 'Y3pNrbVqw3', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('5', 'Jasmin Sawayn V', 'rosetta34@example.net', 'JrpmDjMEIa', '0', '$2y$10$jvTsQEILP1B/dTg79LTctuLzcHUIzxnxVQ3GFCDiY0TEf2XwTpNsK', '0', '9jWjNpFl5l', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('6', 'Dr. Kiera Murphy DVM', 'brando18@example.net', 'y4kMzz7xzX', '0', '$2y$10$YGFkGD6Own7L.np.XBfeauVMhBvvVRitQXf9PU6MzijnB8m/W6TzS', '0', 'SfL8v0oUzU', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('7', 'Kaitlyn O\'Connell', 'antoinette.smith@example.org', '1avKAG0lpt', '0', '$2y$10$cfY7mD2ZFOkNi/68D3ZTbeJ7VO4LFEIHSVYOTAj..LETXwAYA9yBK', '0', 'LrmR39O5Ar', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('8', 'Amari Stokes V', 'yhahn@example.com', 'N2Rbqlb6P4', '0', '$2y$10$yrysjEHW4nuOT7RUFwYC1.29vUbRrQwCwq1.zx1ZYvc2RqoRz12Wi', '0', 'A74WTrU54k', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('9', 'Prof. Enrico O\'Hara', 'chaz.luettgen@example.com', '6VaEP9YM6D', '0', '$2y$10$T77giR3JR8OB76u.R8ePNOWLrOHxZeoFJ/qx9jGrkda3Ac3Xchkcy', '0', '0Zs8vUK8bm', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('10', 'Bill Huels MD', 'candida.daniel@example.com', 'VWChrSWxg0', '0', '$2y$10$iRJrB6057HoOj1JoGfk3X.3GjayE2ti8rxbKQNZXN7EVPgmKtMvmK', '0', 'hTP9WCtcLl', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('11', 'Chesley Wiza', 'llehner@example.net', 'dHb55OVFck', '0', '$2y$10$SpuDbxiMFvoAxfO4XAj4NOUu0.HQgCkUhSYM62nF4Cb6VWk9b/PDu', '0', 'AZtRwXeNE3', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('12', 'Jonas Bahringer', 'jared.schuster@example.org', 'C1OiHZ02wA', '0', '$2y$10$S9zxIZADj8sC6.9BxG8oHuJ4/p1j1NtFMmEzw5uiL2yvv2OQoTQhW', '0', 'OPfQQlg6HP', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('13', 'Lilla Hintz', 'darwin.conroy@example.net', 'CvNOXevLqJ', '0', '$2y$10$naV7fQOOZXPCwPo0jjUDV.g2obiRLS/2KtJ8tR.pw99AqSZ2CI2m6', '0', 'sMa8uuPYxx', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('14', 'Dr. Brando Strosin Jr.', 'kylie12@example.net', '5HIjqPFw5l', '0', '$2y$10$jV5iRZyWgyE6wUGtb5qMG.0IAoyn5CRSZ.8tlPSCzd99EpVZm48uG', '0', 'VlZNn26g2f', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('15', 'Rosemarie Shanahan', 'zfisher@example.com', 'mKvf5Mi7Bd', '0', '$2y$10$Z7hFc02o2xW.dAsOqUaqc.8zbs6AN9r/wot2XGEKUunFOTvseaVgC', '0', 'S0yuHJ8e3w', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('16', 'Ms. Heloise Torp Jr.', 'autumn.kassulke@example.net', 'sJQtqRwj4c', '0', '$2y$10$ripAVc5e0c1i.RgAQ/wv1e1VJqFwZzkgVwK/e7q.iZVHBSCv7EZfK', '0', 'SqU1Sm4iPw', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('17', 'Maymie Dooley', 'yward@example.org', '7QTNfSR1Cr', '0', '$2y$10$eRTDS/K9Fpi5L/9xOaAoLuSWTME6myL9OfSY5eV6XMSVpTo6qc8h2', '0', '2ps9o4YUkb', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('18', 'Thomas Streich', 'marquis.harber@example.net', 'VvuyRqxCOZ', '0', '$2y$10$ARgNV0bq3ZenwnuaNA3truw9HK59WCozXRbWWPC/yToecDgtN2omC', '0', '5AecxXd89c', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('19', 'Annette Schinner', 'lueilwitz.cecil@example.com', 'JrtBKvEDqU', '0', '$2y$10$zeNxbbZGQVAaKzsWiA4gpefk6aANiqFJhn3khpA6rFyIINXU/oAYG', '0', '8PgcXp4THU', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
INSERT INTO `users` VALUES ('20', 'Maurine Luettgen DDS', 'eden.schultz@example.org', 'tb3cArOmJ9', '0', '$2y$10$EKSdz.8Xq5pBpq4ENH5/N.WGuONHq7RQ.eVb8SCBFG5TrE2X/URUy', '0', 'Dy3LxDZztr', '2019-12-21 16:05:30', '2019-12-21 16:05:30');
