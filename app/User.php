<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function blogs(){
        return $this->hasMany(Blog::class);
    }

    //粉丝，查找我的粉丝
    public function follower(){
        return $this->belongsToMany(User::class,'follows','user_id','follower');
    }
    //被关注着，粉丝是主角（获取所有关注）
    public function following(){
        return $this->belongsToMany(User::class,'follows','follower','user_id');
    }

    //是否关注了当前用户（指定用户是否是粉丝）
    public function isFollow($uid){
        return $this->follower()->where('follower',$uid)->first();
    }

    //关注或取消关注
    public function followToggle($ids){
        $ids = is_array($ids)?:[$ids];
        return $this->follower()->withTimestamps()->toggle($ids);
    }
}
