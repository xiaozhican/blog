<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Mail\RegMail;
use App\User;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function home(){
        //$user = User::find(1);
        //\Mail::to($user)->send(new RegMail($user));
        $blogs = Blog::orderBy('id','desc')->with('user')->paginate(5);
        return view('home',compact('blogs'));
    }
}
