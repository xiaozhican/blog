<?php

namespace App\Http\Controllers;

use App\Mail\RegMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',[
            'except'=>['index','show','create','store','confirmEmailToken']//排除方法
        ]);
        $this->middleware('guest',[
            'only'=>['create','store']//只允许游客使用这两个方法
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(10);
        return view('user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $data = $this->validate($request,[
            'name'=>'required|min:3',
            'email'=>'required|unique:users|email',
            'password'=>'required|min:5|confirmed'
        ]);
        $data['password'] = bcrypt($data['password']);
        //添加用户
        $user = User::create($data);
        //自动登录
        //Auth::attempt(['email'=>$request->email,'password'=>$request->password]);
        //发送邮件
        \Mail::to($user)->send(new RegMail($user));
        session()->flash('success','请查看邮箱，完成注册验证');
        return redirect()->route('home');
    }
    //关注或取消关注
    public function follow(User $user){
        $user->followToggle(Auth::user()->id);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $blogs = $user->blogs()->paginate(5);
        if(Auth::check()){
            $followTitle = $user->isFollow(Auth::user()->id)?'取消关注':'关注';
        }
        return view('user.show',compact('user','blogs','followTitle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('update',$user);
        return view('user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $this->validate($request,[
            'name'=>'required|min:3',
            'password'=>'nullable|min:5|confirmed'//nullabel：如果有则验证后面两个，如果没有就不验证，因为修改用户资料时候密码可能不用修改，这个和required冲突
        ]);
        $user->name = $request->name;
        if($request->password){
            $user->password = bcrypt($request->password);
        }
        $user->save();
        session()->flash('success','用户资料修改成功');
        return redirect()->route('user.show',$user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('delete',$user);
        $user->delete();
        session()->flash('success','删除成功');
        return redirect()->route('user.index');
    }
    //邮箱注册验证
    public function confirmEmailToken($token){
        $user = User::where('email_token',$token)->first();
        if($user){
            $user->email_active = true;
            $user->save();
            session()->flash('验证成功');
            Auth::login($user);
            return redirect('/');
        }
        //\Mail::to($user)->send(new RegMail($user));
        session()->flash('danger','邮箱验证失败');
        return redirect('/');
    }
}
