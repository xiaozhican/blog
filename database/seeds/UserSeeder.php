<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class,20)->create();//随机生产2条
        //然后运行数据填充 php artisan db:seed

        /*
        $user = App\User::find(1);//查找id为1的数据
        $user->name = '向军大叔';
        $user->email = '1073298557@qq.com';
        $user->password = bcrypt('123456');
        $user->is_admin = true;
        $user->save();//并更新id=1的数据
        //然后运行数据填充 php artisan db:seed    //表中id=1的数据就更改了
        */
    }
}
